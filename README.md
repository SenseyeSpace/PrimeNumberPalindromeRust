# Palindrome made from the product of two 5-digit prime numbers

##### Result
```
Palindrome: 999949999
Prime numbers 30109 * 33211
Duration: 45 633 014
```

##### Development
```bash
cargo run
cargo build --release
./target/release/PrimeNumberPalindromeRust
```

##### Languages:
* [JavaScript](https://gitlab.com/Senseye/PrimeNumberPalindromeJavaScript)
* [Golang](https://gitlab.com/Senseye/PrimeNumberPalindromeGolang)
* [Python](https://gitlab.com/Senseye/PrimeNumberPalindromePython)
* [C](https://gitlab.com/Senseye/PrimeNumberPalindromeC)
